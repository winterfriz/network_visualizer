function Topology(netjson) {
    this.type = netjson['type'];
    this.label = netjson['label'];
    this.protocol = netjson['protocol'];
    this.version = netjson['version'];
    this.metric = netjson['metric'];
    this.nodes = netjson['nodes'];
    this.links = netjson['links'];
}

// changes status of the definite link
Topology.prototype.change_link_status = function(src, trg, status) {
    for(var i in this.links) {
        if (src == this.links[i]['source'] && trg == this.links[i]['target']) {
            this.links[i]['properties']['status'] = status;
            return
        }
    }
}

Topology.prototype.change_node_state = function(ip, state) {
    for(var i in this.nodes) {
        console.log(node);
        console.log(this.nodes[i]['id']);
        if (ip == this.nodes[i]['id']) {
            this.nodes[i]['properties']['state'] = state;
            this.change_node_links_status(ip, state);
        }
    }
}

Topology.prototype.change_node_links_status = function(ip, state) {
    for(var i in this.links) {
        // print(i)
        if (ip == this.links[i]['source'] || ip == this.links[i]['target']) {
            // if link['properties']['status'] == 'down':
            //     print('add_old_link')
            //     this.old_down_links.append(link)
            // print(state)
            this.links[i]['properties']['status'] = state;
        }
    }
}

Topology.prototype.json = function() {
    var json_topology = {
        'type': this.type,
        'label': this.label,
        'protocol': this.protocol,
        'version': this.version,
        'metric': this.metric,
        'nodes': this.nodes,
        'links': this.links
    }
    return json_topology
}

module.exports = Topology;
