WHAT

**Network_visualizer** is a NodeJS project that can help to display a network topology the finest way.  

The project uses: netjson format, netjsongraph.js library. [NetJSONGraph.js](https://github.com/netjson/netjsongraph.js/) is a js library for visualization network topology.  [NetJSON](https://netjson.org/) – a special JSON format for network.
![1.png](https://bitbucket.org/repo/X844Bg/images/3787451601-1.png)


HOW

First of all there are two mode of network topology: master and update. Master – the current topology that is displayed, update – for history.  To create a topology it needs to send netjson data via POST message. An example:

```
{
"type":"NetworkGraph",
"label":"Kiev_topology",
"protocol":"”,
"version":"",
"metric":"",
"nodes": [
	{
		"id":"172.16.146.99",
		"properties": {
			"state":"up",
	  		"hostname": NAS.nnx",
	  		"node_type":"server"
		}
	},
	{
		"id":"172.16.145.2",
		"properties":{
			"state":"up",
			"hostname":
			"clauzroma.nnx"
		}
	}
],
"links":[
	{
		"source":"172.16.145.2","
		cost":1.2939453125,
		"target":"172.16.146.6",
		"properties":	{
			"status":"down",
			"nlq":0.497,
			"type":"fiber",
			"bitrate":"20 mbit/s",
			"lq":0.9
		}
	}
]
}
```

	
To update the current topology it needs to send part of  link or node netjson with topology name in the head of the message. A way of sending is the same using POST method. Links and nodes can have the three types of connection: up, down, warning.
An example for link:

```
{
"type":"NetworkGraph",
"label":"Kiev_topology",
"links":[
	{
		"source":"172.16.145.2","
		"target":"172.16.146.6",
		"properties":	{
			"status":"up",
		}
	}
]
}
```
There the status is changed.

An example for node:
```
{
"type":"NetworkGraph",
"label":"Kiev_topology",
"nodes": [
	{
		"id":"172.16.146.99",
		"properties": {
			"state":"warning",
	  		"hostname": NAS.nnx",
	  		"node_type":"server"
		}
	}
]
}
```
There the state is changed.

The changes can concern only for node state and link status. Also it is possible to update the all network topology sending the entire netjson data with other data.  It needs to use the same topology name in url and in netjson to update it.

To send a message it is used URLs. Examples for update and master topologies of network:

* [<localhost:9000>/topology/<topology_name>](Link URL)
* [<localhost:9000>/topology/](Link URL)

To see the created topology It needs to enter in browser the url like this:

* [<localhost:9000>/topology/<topology_name>](Link URL)



LINKS

* [https://github.com/netjson/netjsongraph.js/](Lhttps://github.com/netjson/netjsongraph.js/)
* [https://netjson.org/](https://netjson.org/)