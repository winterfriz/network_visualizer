exports.getTopology = function(req, res) {
    res.render('detail', {
        title: 'Topology',
        topology_file: get_topology_fn(req)
    });
};

function get_topology_fn(req) {
    console.log(req.url);
    topology_name = req.url.split('/')[2];
    topology_file_path = '/topologies/'+topology_name+'/'+topology_name+'.json';
    console.log(topology_file_path);
    return topology_file_path;
}

function get_new_json_fn(fn, master) {
    var new_fn = fn.split('.')[0];
    var tale = 'update';
    if (master) {
        tale = 'master';
    }
    var time_int = Date.now();
    console.log(new_fn+"_"+tale+"_"+time_int+".json");
    return new_fn+"_"+tale+"_"+time_int+".json";
}

function update_topology(netjson, topology) {
    if ('nodes' in netjson) {
        for (node in netjson['nodes']) {
            console.log('updating nodes');
            ip = netjson['nodes'][0]['id'];
            state = netjson['nodes'][0]['properties']['state'];
            topology.change_node_state(ip, state);
        }
    } else if ('links' in netjson) {
        for (link in netjson['links']) {
            console.log('updating links');
            src = netjson['links'][0]['source'];
            trg = netjson['links'][0]['target'];
            status = netjson['links'][0]['properties']['status'];
            topology.change_link_status(src, trg, status);
        }
    }
}

function processTopology(data, master) {
    console.log('processTopology');
    console.log(data);
    var netjson_dict_new = JSON.parse(data);
    console.log(netjson_dict_new.label);
    console.log();

    var top_name = netjson_dict_new.label;
    var path_cur = require('path').dirname(require.main.filename);
    var path_topol = path_cur + '/public/topologies/' + top_name + '/';
    console.log(path_topol);
    var new_dir = path_topol + top_name;
    var new_fn = new_dir + '.json';
    console.log(new_fn);
    var new_fn_master = new_dir + '_master.json';
    var new_dir_history = path_topol + 'history/';
    var new_fn_history = new_dir_history + top_name + '.json';

    console.log(master);
    console.log(path_topol);
    console.log(new_dir_history);
    if (master==true) {
        var fs = require('fs');
        if (!fs.existsSync(path_topol)) {
            var mkdirp = require('mkdirp');
            mkdirp(path_topol);
            mkdirp(new_dir_history);
        }

        fs.writeFile(new_fn, JSON.stringify(netjson_dict_new));

        fs.writeFile(new_fn_master, JSON.stringify(netjson_dict_new));

        if (fs.lstatSync(new_fn).isFile()) {
            fs.createReadStream(new_fn_master).pipe(fs.createWriteStream(get_new_json_fn(new_fn_history, master)));
            console.log('move old master netjson file to history');
        }

        return
    }
    var fs = require('fs');
    var netjson_dict_old = JSON.parse(fs.readFileSync(new_fn));
    console.log(netjson_dict_old.toString());
    var Topology = require('./topology');
    var topology = new Topology(netjson_dict_old);

    update_topology(netjson_dict_new, topology);
    var netjson_dict_upd = topology.json();

    var fs = require('fs');
    var fs = require('fs');

    console.log(new_fn);
    console.log(JSON.stringify(netjson_dict_upd));
    fs.writeFile(new_fn, JSON.stringify(netjson_dict_upd));

    console.log(get_new_json_fn(new_fn_history, master));
    fs.createReadStream(new_fn).pipe(fs.createWriteStream(get_new_json_fn(new_fn_history, master)));

    console.log('file netjson updated');
}

module.exports.processTopology = processTopology;
