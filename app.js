var express = require("express");
var fs = require("fs");
var path = require('path');
var app = express();
var receiveTopologyViev = require('./receiveTopologyView');


app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');


app.use(express.static(__dirname + '/public'));


app.listen(9000,function(){
    console.log("Started on PORT 9000");
});

app.get('/topology/*', receiveTopologyViev.getTopology);


app.post('/receive/*',function(req,res) {
    req.on('data', function(chunk) {
                console.log("Received body data:");
                console.log(chunk.toString());
                var rtv;
                var data = chunk.toString();

                if (req.url === '/receive/') {
                    rtv = receiveTopologyViev.processTopology(data, master=false);
                } else if (req.url === '/receive/master/') {
                    rtv = receiveTopologyViev.processTopology(data, master=true);
                }
            });
            req.on('end', function() {
                // empty 200 OK response for now
                res.writeHead(200, "OK", {'Content-Type': 'text/html'});
                res.end();
            });
});

